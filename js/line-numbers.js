// Add line numbers to paragraphs 
// to make correction in source code easyier
// works with line-number-post.js and interface.css
// to display line numbers

// deactivate adding preview class to body
// add a data-body-line-number="n" attribute to body tag
// where n is its line number

// this script calculates the line numbers from the dom 
// and NOT from the source file itself
// so we'll base count on body tag as pagedjs 
// adds many lines in the head section
// this requires a delta (number of lines in the original head
//  - or <body> element line number
// to be acurate

// add to your interface.css
// .line-number {
//   display: block !important;
//   font-size: 0.8rem;
//   text-align: left;
//   color: var(--color-line-numbers);
//   position: absolute;
//   width: var(--outer-margin);
//   height: 1.5em;
//   right: calc(var(--outer-margin) * -1);
//   top: 0;
//   left: auto;
//   padding: 0 20px;
//   text-indent: 0;
// }
// .pagedjs_left_page .line-number {
//   left: calc(var(--outer-margin) * -1);
//   right: auto;
//   text-align: right;
// }

console.log('adding line numbers');
var startTime = performance.now()

// stop pagedjs
window.PagedConfig = {
  auto: false
};
document.body.style.display = 'none';

window.onload = () => {
  
  var theBody = document.body
  const delta = parseInt(theBody.getAttribute('data-body-line-number'));
  
  if (!theBody.classList.contains("preview")) {
    
    // prepare to count : convert to text and count lines

    // convert to string
    var rootElement = theBody.outerHTML;
    
    // split lines
    const splitLines =  rootElement.split(/\r?\n/);
    
    function addLinesNumber(value, index, array) {
      // focusing on paragraphs to be fast
      var tag = "p"
      var thisLine = index + delta
      txt += value.replace('<'+tag, '<'+tag+' data-line-number="' + thisLine + '"')
    };
    
    let txt = "";
    const numbered = splitLines.forEach(addLinesNumber)
    
    var parser = new DOMParser();
    var parsed = parser.parseFromString(txt, "text/html");  
    
    if (typeof parsed === "object") {
      var newBody = parsed.body;
      theBody.parentNode.replaceChild(newBody, theBody);
      var endTime = performance.now();
      var deltaTime = endTime - startTime;
      console.log("Adding Line numbers took " + deltaTime + " milliseconds");
      setTimeout(function () {
        document.body.removeAttribute("style");
        window.PagedPolyfill.preview();
        setTimeout(function(){ 
          window.moveFast();
        }, 10);
      }, deltaTime); // delay before triggering PagedJS
            
    } else {
      console.log('pas objet')
    }
  } else {
    // remove crop marks - does not work !
    var css = '@media print {@page {marks: none !important;}}',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
    head.appendChild(style);
    style.appendChild(document.createTextNode(css));

    // wait and launch pagedjs
    var endTime = performance.now();
    var deltaTime = endTime - startTime;
    console.log("Adding Line numbers took " + deltaTime + " milliseconds");
    setTimeout(function () {
      document.body.removeAttribute("style");
      window.PagedPolyfill.preview();
      setTimeout(function(){ 
        window.moveFast();
      }, 10);
    }, deltaTime); // delay before triggering PagedJS
  }
}

