let autoscrollActive = false;
let nbOfColumns = 3;
let gap = 5;


let guidelines = false;
// Show / Hide Guidelines by ctrl + b
document.body.addEventListener('keydown', function(e){
  // e.preventDefault()
  let target = e.target;
  let margins = document.querySelectorAll('.pagedjs_page_content');
  let cols = document.querySelectorAll('.gui-column');
  if (e.ctrlKey && e.key == 'b') { 
    if(guidelines){
      console.log("Hide guidelines");
      guidelines = false;
      margins.forEach(function(el, i) {
        el.style.outline = "none";
      });
      cols.forEach(function(col, i) {
        col.style.outline = "none";
      }); 
    }
    else{
      console.log("Display guidelines");
      guidelines = true;
      margins.forEach(function(el, i) {
        el.style.outline = "1px solid magenta";
      });
      cols.forEach(function(col, i) {
        col.style.outline = "1px solid cyan";
      });
    }

    e.preventDefault();
  }
});


class toc extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    beforeParsed(content){         
      createToc({
        content: content,
        tocElement: '#table-of-contents',
        titleElements: [ '.chapter h2','.chapter h3' ]
      });
    }
    
  }
Paged.registerHandlers(toc);


// TOC
function createToc(config){
    const content = config.content;
    const tocElement = config.tocElement;
    const titleElements = config.titleElements;
    
    let tocElementDiv = content.querySelector(tocElement);
    let tocUl = document.createElement("ul");
    tocUl.id = "list-toc-generated";
    tocElementDiv.appendChild(tocUl); 
    // console.log(tocElementDiv);

    // add class to all title elements
    let tocElementNbr = 0;
    for(var i= 0; i < titleElements.length; i++){
        	
        let titleHierarchy = i + 1;
        let titleElement = content.querySelectorAll(titleElements[i]);


        titleElement.forEach(function(element) {
            // add classes to the element
            element.classList.add("title-element");
            element.setAttribute("data-title-level", titleHierarchy);

            // add id if doesn't exist
            tocElementNbr++;
            let idElement = element.id;
            if(idElement == ''){
                element.id = 'title-element-' + tocElementNbr;
            } 
            let newIdElement = element.id;

        });

    }

    // create toc list
    let tocElements = content.querySelectorAll(".title-element");  

    for(var i= 0; i < tocElements.length; i++){
        let tocElement = tocElements[i];

        let tocNewLi = document.createElement("li");

        // Add class for the hierarcy of toc
        tocNewLi.classList.add("toc-element");
        tocNewLi.classList.add("toc-element-level-" + tocElement.dataset.titleLevel);

        // Keep class of title elements
        let classTocElement = tocElement.classList;
        for(var n= 0; n < classTocElement.length; n++){
            if(classTocElement[n] != "title-element"){
                tocNewLi.classList.add(classTocElement[n]);
            }   
        }
        // Create the element
        tocNewLi.innerHTML = '<a href="#' + tocElement.id + '">' + tocElement.innerHTML + '</a>';
        tocUl.appendChild(tocNewLi);  
    }

}

// add columns grid layout (for developpement)
class guiCol extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered(pages) {
    for(let i=0; i<pages.length; i++){
      let colWrapper = document.createElement("div");
      colWrapper.classList.add('col-wrapper');
      for(let a=0; a<nbOfColumns; a++){
        let col = document.createElement("div");
        col.classList.add('gui-column');
        let widthInPercent = 100 / nbOfColumns;
        col.style.width = "calc("+widthInPercent+"% - calc("+gap+"mm /2))";
        colWrapper.appendChild(col);
      }
      pages[i].area.appendChild(colWrapper);
    }
  }
}

Paged.registerHandlers(guiCol);


class autoscroll extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    // Autoscrooll and reload

    let scrollSpeed = 1;
    let scrollInterval = 5;
    let bottom = document.querySelector('#backcover')
    function autoScrollAndReload() {
      window.scrollBy(0, scrollSpeed)
      console.log(isScrolledIntoView(bottom));
      if (isScrolledIntoView(bottom)) {
        setTimeout(() => {
          location.reload();          
        }, 15000);
        return
      }
    }
    if (autoscrollActive) {
      setTimeout(function(){
        let scrollingInterval = setInterval(autoScrollAndReload, scrollInterval);
      }, 1000);  
    }

  }
}
Paged.registerHandlers(autoscroll);


function isScrolledIntoView(el) {
  var rect = el.getBoundingClientRect();
  var elemTop = rect.top;
  var elemBottom = rect.bottom;
  var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
  return isVisible;
}
