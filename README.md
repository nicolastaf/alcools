# Alcools


Projet de mise en forme de la poésie en vers avec CSS, version paginée avec [PagedJS](https://pagedjs.org).

Expérience menée par @euji_ et @nicolastaf lors de la session [PPPrototypes](http://prepostprint.org) 2024.

Licence [Art Libre](https://artlibre.org/).

